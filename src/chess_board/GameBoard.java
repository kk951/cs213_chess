package chess_board;

import java.util.ArrayList;
import java.util.Scanner;
import chess_pieces.*;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */


/**
 * 
 * GameBoard Class
 * 
 * This is where the game logic will go and where we set/update the board.
 *  
 */

public class GameBoard {
	
	/**
	 * Piece board[][] is where the game the game pieces will be stored and presented to the user.
	 */
	static Piece board[][] = new Piece[9][9];
	
	/**
	 * int GCounter variable keeps track of how many times the board has been presented for each white player and black player.
	 */
	int GCounter = 0;
	
	/**
	 * int origin[] keeps track of the original spot that the piece starts at with the first two characters of the filerank.
	 */
	static int origin[] = new int[2]; 
	
	/**
	 * int newP[] keeps track of the new spot that the piece is going to move to with the two characters after the space of the filerank.
	 */
	static int newP[] = new int[2];
	
	/**
	 * boolean whiteMove keeps track of if its white's move or black's move.
	 */
	static boolean whiteMove = false;
	
	/**
	 * boolean blackMove keeps track of its black's move or white's move.
	 */
	static boolean blackMove = false;
	/**
	 * boolean wIncheck keeps track if the white king is being threatened. 
	 */
	public static boolean wIncheck;
	/**
	 * boolean bIncheck keeps track if the black king is being threatened. 
	 */
	public static boolean bIncheck;
	
	/**
	 * int[] wKPosition tracks the position of the White King on the board.
	 */
	static int[] wKPosition = new int[2];
	
	/**
	 * int[] bKPosition tracks the position of the Black King on the board 
	 */
	static int[] bKPosition = new int[2];
	
	/**
	 * boolean blackCheckmate checks if black has checkmated white
	 */
	static boolean blackCheckmate; 
	
	/**
	 * boolean whiteCheckmate checks if white has checkmated black
	 */
	static boolean whiteCheckmate; 
	
	/**
	 * When the pawn is eligible for promotion, stores the inputted letter that indicates which piece the user
	 * would like to promote their pawn to.
	 */
	static char promotion;
	
	/**
	 * Generates the initial board at the start of every game.
	 */
	public static void generateBoard() {
		int length = board.length-1;
		for(int i = 0; i <= board.length-1; i++) {
			for(int j = 0; j <= board.length-1; j++) {
				if(j == 8 && length != 0) {
					board[i][j] = new EmptySpace(Integer.toString(length--), false);
				} else if(i == 8 && j != 8) {
					board[8][0] = new EmptySpace(" a ", false);
					board[8][1] = new EmptySpace(" b ", false);
					board[8][2] = new EmptySpace(" c ", false);
					board[8][3] = new EmptySpace(" d ", false);
					board[8][4] = new EmptySpace(" e ", false);
					board[8][5] = new EmptySpace(" f ", false);
					board[8][6] = new EmptySpace(" g ", false);
					board[8][7] = new EmptySpace(" h ", false);
				} else if((i >= 2 && i <= 5) && j != 8){
					if(i == 2 && j != 8) {
						board[i][0] = new EmptySpace("   ", false);
						board[i][1] = new EmptySpace("## ", false);
						board[i][2] = new EmptySpace("   ", false);
						board[i][3] = new EmptySpace("## ", false);
						board[i][4] = new EmptySpace("   ", false);
						board[i][5] = new EmptySpace("## ", false);
						board[i][6] = new EmptySpace("   ", false);
						board[i][7] = new EmptySpace("## ", false);
					} else if(i == 3 && j != 8){
						board[i][0] = new EmptySpace("## ", false);
						board[i][1] = new EmptySpace("   ", false);
						board[i][2] = new EmptySpace("## ", false);
						board[i][3] = new EmptySpace("   ", false);
						board[i][4] = new EmptySpace("## ", false);
						board[i][5] = new EmptySpace("   ", false);
						board[i][6] = new EmptySpace("## ", false);
						board[i][7] = new EmptySpace("   ", false);
					} else if(i == 4 && j != 8) {
						board[i][0] = new EmptySpace("   ", false);
						board[i][1] = new EmptySpace("## ", false);
						board[i][2] = new EmptySpace("   ", false);
						board[i][3] = new EmptySpace("## ", false);
						board[i][4] = new EmptySpace("   ", false);
						board[i][5] = new EmptySpace("## ", false);
						board[i][6] = new EmptySpace("   ", false);
						board[i][7] = new EmptySpace("## ", false);
					} else if(i ==5 && j!= 8) {
						board[i][0] = new EmptySpace("## ", false);
						board[i][1] = new EmptySpace("   ", false);
						board[i][2] = new EmptySpace("## ", false);
						board[i][3] = new EmptySpace("   ", false);
						board[i][4] = new EmptySpace("## ", false);
						board[i][5] = new EmptySpace("   ", false);
						board[i][6] = new EmptySpace("## ", false);
						board[i][7] = new EmptySpace("   ", false);
					}
				} else if((i == 0 || i == 1 || i == 6 || i == 7) && j != 8) {
					if(i == 0 && j != 8) {
						board[i][0] = new Rook("bR ", false);
						board[i][1] = new Knight("bN ", false);
						board[i][2] = new Bishop("bB ", false);
						board[i][3] = new Queen("bQ ", false);
						board[i][4] = new King("bK ", false);
						board[i][5] = new Bishop("bB ", false);
						board[i][6] = new Knight("bN ", false);
						board[i][7] = new Rook("bR ", false);
					} else if(i == 1 && j != 8){
						board[i][0] = new Pawn("bp ", false, false);
						board[i][1] = new Pawn("bp ", false, false);
						board[i][2] = new Pawn("bp ", false, false);
						board[i][3] = new Pawn("bp ", false, false);
						board[i][4] = new Pawn("bp ", false, false);
						board[i][5] = new Pawn("bp ", false, false);
						board[i][6] = new Pawn("bp ", false, false);
						board[i][7] = new Pawn("bp ", false, false);
					} else if(i == 6 && j != 8){
						board[i][0] = new Pawn("wp ", true, false);
						board[i][1] = new Pawn("wp ", true, false);
						board[i][2] = new Pawn("wp ", true, false);
						board[i][3] = new Pawn("wp ", true, false);
						board[i][4] = new Pawn("wp ", true, false);
						board[i][5] = new Pawn("wp ", true, false);
						board[i][6] = new Pawn("wp ", true, false);
						board[i][7] = new Pawn("wp ", true, false);
					} else if(i == 7 && j != 8){
						board[i][0] = new Rook("wR ", true);
						board[i][1] = new Knight("wN ", true);
						board[i][2] = new Bishop("wB ", true);
						board[i][3] = new Queen("wQ ", true);
						board[i][4] = new King("wK ", true);
						board[i][5] = new Bishop("wB ", true);
						board[i][6] = new Knight("wN ", true);
						board[i][7] = new Rook("wR ", true);
					} else {
						board[i][j] = new EmptySpace(" - ", false);
					}					
				} if(i == 8 && j == 8) {
					board[i][j] = new EmptySpace("", false);
				}
			}
		}
		printBoard(board);
	}
	
	/**
	 * Prints the game board for the initial step or any update that happens.
	 * @param board
	 */
	public static void printBoard(Piece board[][]) {
		for(int i = 0; i <= board.length - 1; i++) {
			for(int j = 0; j <= board.length - 1; j++) {
				System.out.print(board[i][j]);
			}
			System.out.println();
		}
	}
	 
	/**
	 * Stores the origin and the new position of the piece in movement through the filerank.
	 * 
	 * @param origin
	 * @param newP
	 * @param FileRank
	 */
	private static void toIndex(int origin[], int newP[], String FileRank) {
		
		origin[0] = Character.getNumericValue(FileRank.charAt(1));
		
		if(origin[0] == 1) {
			origin[0] = 7;
		} else if(origin[0] == 2) {
			origin[0] = 6;
		} else if(origin[0] == 3) {
			origin[0] = 5;
		} else if(origin[0] == 4) {
			origin[0] = 4;
		} else if(origin[0] == 5) {
			origin[0] = 3;
		} else if(origin[0] == 6) {
			origin[0] = 2;
		} else if(origin[0] == 7) {
			origin[0] = 1;
		} else if(origin[0] == 8) {
			origin[0] = 0;
		}
		
		origin[1] = Character.getNumericValue(FileRank.charAt(0)) - 10;
		
		newP[0] = Character.getNumericValue(FileRank.charAt(4));
		
		if(newP[0] == 1) {
			newP[0] = 7;
		} else if(newP[0] == 2) {
			newP[0] = 6;
		} else if(newP[0] == 3) {
			newP[0] = 5;
		} else if(newP[0] == 4) {
			newP[0] = 4;
		} else if(newP[0] == 5) {
			newP[0] = 3;
		} else if(newP[0] == 6) {
			newP[0] = 2;
		} else if(newP[0] == 7) {
			newP[0] = 1;
		} else if(newP[0] == 8) {
			newP[0] = 0;
		}
		
		newP[1] = Character.getNumericValue(FileRank.charAt(3)) - 10;
	}
	
	/**
	 * Checks if given indices are within the bounds of the 8x8 chess board.
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public static boolean inbounds(int row, int col) {
		if((row >= 0 && row <= 7) && (col >= 0 && col <= 7)) {
			return true; 
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if a enPassant is present for the pawn pieces and updates that in the board.
	 * 
	 * @param boardDup
	 * @return boardDup
	 */
	public static Piece[][] enPassantChecker(Piece boardDup[][]) {
		for(int i = 0; i <= boardDup.length - 1; i++) {
			for(int j = 0; j <= boardDup.length - 1; j++) {				
				if(i == 3) {
					if(boardDup[i][j].toString().equals("bp ")) {
						boardDup[i][j].enpassant = true;
					} else {
						boardDup[i][j].enpassant = false;
					}
				} else if(i == 4) {
					if(boardDup[i][j].toString().equals("wp ")) {
						boardDup[i][j].enpassant = true;
					} else {
						boardDup[i][j].enpassant = false;
					}
				} 
			}
		}
		return boardDup;
	}
	
	/**
	 * This is where checking whether the current player has checkmated their opponent commences.
	 */
	public static void checkmate() {
		Piece king; 
		int rank, file;
		boolean threatIsWhite;
		Piece opponent = null;
		Piece threat = null; 
		int[] threatPosition = new int[2];
		int i, j;
		String direction = null;
		if (blackMove) {
			king = board[wKPosition[0]][wKPosition[1]];
			rank = wKPosition[0];
			file = wKPosition[1];
			threatIsWhite = false; 
		}
		else { //whiteMove
			king = board[bKPosition[0]][bKPosition[1]];
			rank = bKPosition[0];
			file = bKPosition[1];
			threatIsWhite = true; 
		}
			
		/* Check straight lines */
		for (i = rank + 1; i < 8; i++) { //down
			opponent = board[i][file];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R')) {
				threat = opponent;
				threatPosition[0] = i; 
				threatPosition[1] = file;
				direction = "down";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, file, board))
				break; 
		}
		
		for (i = rank - 1; i >= 0; i--) { //up
			opponent = board[i][file];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R')) {
				threat = opponent; 
				threatPosition[0] = i; 
				threatPosition[1] = file;
				direction = "up";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, file, board))
				break; 
		}
		for (i = file + 1; i < 8; i++) { //right
			opponent = board[rank][i];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R')) {
				threat = opponent; 
				threatPosition[0] = rank; 
				threatPosition[1] = i;
				direction = "right";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(rank, i, board))
				break; 
		}
		for (i = file - 1; i >= 0; i--) { //left
			opponent = board[rank][i];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R')) {
				threat = opponent; 
				threatPosition[0] = rank; 
				threatPosition[1] = i;
				direction = "left";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(rank, i, board))
				break; 
		}
		
		/* Check diagonals */
		for (i = rank - 1, j = file - 1; GameBoard.inbounds(i, j); i--, j--) { //left-up
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B')) {
				threat = opponent; 
				threatPosition[0] = i; 
				threatPosition[1] = j;
				direction = "left-up";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		for (i = rank + 1, j = file - 1; GameBoard.inbounds(i, j); i++, j--) { //left-down
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B')) {
				threat = opponent; 
				threatPosition[0] = i; 
				threatPosition[1] = j;
				direction = "left-down";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		for (i = rank - 1, j = file + 1; GameBoard.inbounds(i, j); i--, j++) { //right-up
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B')) {
				threat = opponent; 
				threatPosition[0] = i; 
				threatPosition[1] = j;
				direction = "right-up";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		for (i = rank + 1, j = file + 1; GameBoard.inbounds(i, j); i++, j++) { //right-down
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B')) {
				threat = opponent; 
				threatPosition[0] = i; 
				threatPosition[1] = j;
				direction = "right-down";
				break;
			}
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		
		/* Check for potentially threatening pawns */
			if (GameBoard.inbounds(rank+1, file+1) && board[rank+1][file+1].name.equals("wp ")) {
				threat = board[rank+1][file+1];
				threatPosition[0] = rank+1; 
				threatPosition[1] = file+1;
			}
			if (GameBoard.inbounds(rank+1, file-1) && board[rank+1][file-1].name.equals("wp ")) {
				threat = board[rank+1][file-1];
				threatPosition[0] = rank+1; 
				threatPosition[1] = file-1;
			}
		
		/* Check for knights */
		if(GameBoard.inbounds(rank-2, file+1) && board[rank-2][file+1].name.charAt(1) == 'N' && king.isWhite != board[rank-2][file+1].isWhite) {
			threat = board[rank-2][file+1];
			threatPosition[0] = rank-2; 
			threatPosition[1] = file+1;
		}
				
		if(GameBoard.inbounds(rank-2, file-1) && board[rank-2][file-1].name.charAt(1) == 'N' && king.isWhite != board[rank-2][file-1].isWhite) {
			threat = board[rank-2][file-1];
			threatPosition[0] = rank-2; 
			threatPosition[1] = file-1;
		}

		if(GameBoard.inbounds(rank+2, file+1) && board[rank+2][file+1].name.charAt(1) == 'N' && king.isWhite != board[rank+2][file+1].isWhite) {
			threat = board[rank+2][file+1];
			threatPosition[0] = rank+2; 
			threatPosition[1] = file+1;
		}

		if(GameBoard.inbounds(rank+2, file-1) && board[rank+2][file-1].name.charAt(1) == 'N' && king.isWhite != board[rank+2][file-1].isWhite) {
			threat = board[rank+2][file-1];
			threatPosition[0] = rank+2; 
			threatPosition[1] = file-1;
		}

		if(GameBoard.inbounds(rank+1, file-2) && board[rank+1][file-2].name.charAt(1) == 'N' && king.isWhite != board[rank+1][file-2].isWhite) {
			threat = board[rank+1][file-2];
			threatPosition[0] = rank+1; 
			threatPosition[1] = file-2;
		}

		if(GameBoard.inbounds(rank-1, file-2) && board[rank-1][file-2].name.charAt(1) == 'N' && king.isWhite != board[rank-1][file-2].isWhite) {
			threat = board[rank-1][file-2];
			threatPosition[0] = rank-1; 
			threatPosition[1] = file-2;
		}

		if(GameBoard.inbounds(rank+1, file+2) && board[rank+1][file+2].name.charAt(1) == 'N' && king.isWhite != board[rank+1][file+2].isWhite) {
			threat = board[rank+1][file+2];
			threatPosition[0] = rank+1; 
			threatPosition[1] = file+2;
		}

		if(GameBoard.inbounds(rank-1, file+2) && board[rank-1][file+2].name.charAt(1) == 'N' && king.isWhite != board[rank-1][file+2].isWhite) {
			threat = board[rank-1][file+2];
			threatPosition[0] = rank-1; 
			threatPosition[1] = file+2;
		}
		
		if(threat != null) {
			boolean kill = King.checked(board, threatPosition[0], threatPosition[1], threatIsWhite);
			if (kill) {
				return;
			}
			else { 
				boolean block = block(threat, threatPosition[0], threatPosition[1], rank, file, direction);
				if (block)
					return;
				else { //last stage
					boolean checked = false;
					if (blackMove) {
						if (GameBoard.inbounds(rank-1, file+1) && (board[rank-1][file+1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank-1, file+1, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank-1, file-1) && (board[rank-1][file-1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank-1, file-1, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank+1, file+1) && (board[rank+1][file+1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank+1, file+1, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank+1, file-1) && (board[rank+1][file-1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank+1, file-1, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank, file+1) && (board[rank][file+1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank, file+1, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank, file-1) && (board[rank][file-1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank, file-1, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank-1, file) && (board[rank-1][file].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank-1, file, true);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank+1, file) && (board[rank+1][file].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank+1, file, true);
							if(!checked)
								return;
						}
					}
					else { //whiteMove
						if (GameBoard.inbounds(rank-1, file+1) && (board[rank-1][file+1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank-1, file+1, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank-1, file-1) && (board[rank-1][file-1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank-1, file-1, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank+1, file+1) && (board[rank+1][file+1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank+1, file+1, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank+1, file-1) && (board[rank+1][file-1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank+1, file-1, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank, file+1) && (board[rank][file+1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank, file+1, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank, file-1) && (board[rank][file-1].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank, file-1, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank-1, file) && (board[rank-1][file].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank-1, file, false);
							if(!checked)
								return;
						}
						if (GameBoard.inbounds(rank+1, file) && (board[rank+1][file].name.equals("## ") || board[rank-1][file+1].name.equals("   "))) {
							checked = King.checked(board, rank+1, file, false);
							if(!checked)
								return;
						}
					}
					if (blackMove && checked)
						blackCheckmate = true; 
					else if (whiteMove && checked)
						whiteCheckmate = true;
				}
			}
		}
		return;
	}
	/**
	 * This is a supplementary method to checkmate(),
	 * which is used to convert the Integer type position coordinates 
	 * of a friendly piece to an int type.
	 * @param arr	
	 * @return converted integer array of position coordinates				
	 */
	public static int[] convert(Integer[] arr) {
		int[] result = new int[2];
		result[0] = arr[0].intValue();
		result[1] = arr[1].intValue();
		return result;
	}
	/**
	 * Supplementary method to checkmate(), used to check whether a friendly piece 
	 * is able to block an opponent piece threatening the king.
	 * @param threat
	 * @param rank
	 * @param file
	 * @param kingRank
	 * @param kingFile
	 * @param direction
	 * @return true if a block is possible, false if not
	 */
	public static boolean block(Piece threat, int rank, int file, int kingRank, int kingFile, String direction) {
		// only queen, rook, and bishop can be blocked from a distance away
		if (threat.name.charAt(1) == 'N' || threat.name.charAt(1) == 'p')
			return false;
		if (Math.abs(rank-kingRank) >= 2 || Math.abs(file-kingFile) >= 2) {
			ArrayList<Integer[]> pieces = getRemainingPieces();
			if (direction.equals("up")) {
				for(int i = kingRank - 1; i > rank; i--) {
					int[] square = {i, file};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("down")) {
				for(int i = kingRank + 1; i < rank; i++) {
					int[] square = {i, file};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("left")) {
				for(int i = kingFile - 1; i > file; i--) {
					int[] square = {rank, i};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("right")) {
				for(int i = kingFile + 1; i < file; i++) {
					int[] square = {rank, i};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("left-up")) {
				for(int i = kingRank - 1, j = kingFile - 1; i > rank; i--, j--) {
					int[] square = {i, j};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("left-down")) {
				for(int i = kingRank + 1, j = kingFile - 1; i < rank; i++, j--) {
					int[] square = {i, j};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("right-up")) {
				for(int i = kingRank - 1, j = kingFile + 1; i > rank; i--, j++) {
					int[] square = {i, j};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
			
			if (direction.equals("right-down")) {
				for(int i = kingRank + 1, j = kingFile + 1; i < rank; i++, j++) {
					int[] square = {i, j};
					for(Integer[] piece : pieces) {
						int[] position = convert(piece);
						if (board[piece[0]][piece[1]].isValidMove(board, position, square))
							return true;
					}	
				}
			}
		}
		else {
			return false;
		}
		return false;
	}
	/**
	 * Supplementary method to checkmate(), which checks for all remaining friendly pieces on the board.
	 * @return	ArrayList of position coordinates to remaining friendly pieces
	 */
	public static ArrayList<Integer[]> getRemainingPieces() {
		ArrayList<Integer[]> result = new ArrayList<Integer[]>();
		Integer[] temp = new Integer[2];
		if (blackMove) { //black is threatening, so must scan for white pieces
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (board[i][j].name.charAt(0) == 'w' && !board[i][j].name.equals("wK ")) {
						temp[0] = i; temp[1] = j;
						result.add(temp);
					}
				}
			}
		}
		else {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (board[i][j].name.charAt(0) == 'b' && !board[i][j].name.equals("bK ")) {
						temp[0] = i; temp[1] = j;
						result.add(temp);
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * This is where the logic of the game is placed and determines if a move is valid based on the file rank. 
	 * 
	 * @param FileRank
	 * @return true or false depending on if the given FileRank is valid for the piece
	 */
	public static boolean movePiece(String FileRank) { 
					
		toIndex(origin, newP, FileRank);
		
		board = enPassantChecker(board);
		
		Piece originPiece = board[origin[0]][origin[1]];
		
		if(originPiece.toString().equals("   ") || originPiece.toString().equals("## ")) {
			System.out.println("Illegal move, try again");
			return false;
		}
		
		if(blackMove) {
			if(originPiece.name.charAt(0) == 'w') {
				System.out.println("Illegal move, try again");
				return false; 
			}
		} else if(whiteMove) {
			if(originPiece.name.charAt(0) == 'b') {
				System.out.println("Illegal move, try again");
				return false; 
			}
		}
		
		Piece endPiece = board[newP[0]][newP[1]];
		
		if(blackMove) {
			if(originPiece.isValidMove(board, origin, newP)) {
				if(inbounds(newP[0]-1, newP[1]) && board[newP[0]-1][newP[1]].enpassant == true) {
					if(!((newP[0]-1)%2 == 0)) {
						if(!(newP[1]%2==0)) {
							board[newP[0]-1][newP[1]] = new EmptySpace("   ", false);
						} else {
							board[newP[0]-1][newP[1]] = new EmptySpace("## ", false);
						}
					} else if((newP[0]-1)%2 == 0 || (newP[0]-1) == 0){
						if(!(newP[1]%2==0)) {
							board[newP[0]-1][newP[1]] = new EmptySpace("## ", false);
						} else {
							board[newP[0]-1][newP[1]] = new EmptySpace("   ", false);
						}
					}
				}
				if (originPiece.name.equals("bK ")) {
					wIncheck = King.checked(board, wKPosition[0], wKPosition[1], true);
					bIncheck = King.checked(board, newP[0], newP[1], false);
					if (bIncheck) {
						System.out.println("Illegal move, try again"); 
						return false;
					}
					else {
						bKPosition[0] = newP[0];
						bKPosition[1] = newP[1];
						board[newP[0]][newP[1]] = originPiece;
						checkmate();
						if (blackCheckmate)
							return true;
					}
				}
				else {
					Piece temp = endPiece; 
					board[newP[0]][newP[1]] = originPiece;
					checkmate();
					if (blackCheckmate)
						return true; 
					wIncheck = King.checked(board, wKPosition[0], wKPosition[1], true);
					bIncheck = King.checked(board, bKPosition[0], bKPosition[1], false);
					if (bIncheck) {
						board[newP[0]][newP[1]] = temp;
						System.out.println("Illegal move, try again");
						return false;
					}
				}	
				if (originPiece.name.charAt(1) == 'p' && (newP[0] == board.length-1 || newP[0] == 0)) { 
					if (FileRank.length() == 5) {
						promotion = 'Q';
						Pawn pawn = (Pawn) originPiece;
						pawn.promotion(newP[0], newP[1], board, promotion);
					}
					else {
						promotion = FileRank.charAt(6);
						Pawn pawn = (Pawn) originPiece;
						pawn.promotion(newP[0], newP[1], board, promotion);
					}
				}
				if(!(origin[0] % 2 == 0)) {
					if(!(origin[1] % 2 == 0)) {
						board[origin[0]][origin[1]] = new EmptySpace("   ", false);
					} else {
						board[origin[0]][origin[1]] = new EmptySpace("## ", false);
					}
				} else if(origin[0] % 2 == 0 || origin[0] == 0){
					if(!(origin[1] % 2 == 0)) {
						board[origin[0]][origin[1]] = new EmptySpace("## ", false);
					} else {
						board[origin[0]][origin[1]] = new EmptySpace("   ", false);
					}
				}
				printBoard(board);
			} else {
				System.out.println("Illegal move, try again");
				return false; 
			}
		} else {
			if(originPiece.isValidMove(board, origin, newP)) {
				if(board[newP[0]+1][newP[1]].enpassant == true) {
					if(!((newP[0]+1)%2 == 0)) {
						if(!(newP[1]%2==0)) {
							board[newP[0]+1][newP[1]] = new EmptySpace("   ", false);
						} else {
							board[newP[0]+1][newP[1]] = new EmptySpace("## ", false);
						}
					} else if((newP[0]+1)%2 == 0 || (newP[0]+1) == 0){
						if(!(newP[1]%2==0)) {
							board[newP[0]+1][newP[1]] = new EmptySpace("## ", false);
						} else {
							board[newP[0]+1][newP[1]] = new EmptySpace("   ", false);
						}
					}
				}
				if (originPiece.name.equals("wK ")) {
					wIncheck = King.checked(board, newP[0], newP[1], true);
					bIncheck = King.checked(board, bKPosition[0], bKPosition[1], false);
					if (wIncheck) {
						System.out.println("Illegal move, try again");
						return false;
					}
					else {
						wKPosition[0] = newP[0];
						wKPosition[1] = newP[1];
						board[newP[0]][newP[1]] = originPiece;
						checkmate();
						if (whiteCheckmate)
							return true; 
					}
				}
				else {
					Piece temp = endPiece;
					board[newP[0]][newP[1]] = originPiece;
					checkmate();
					if (whiteCheckmate)
						return true; 
					wIncheck = King.checked(board, wKPosition[0], wKPosition[1], true);
					bIncheck = King.checked(board, bKPosition[0], bKPosition[1], false);
					if (wIncheck) {
						board[newP[0]][newP[1]] = temp;
						System.out.println("Illegal move, try again");
						return false;
					}
				}
				
				if (originPiece.name.charAt(1) == 'p' && (newP[0] == board.length-1 || newP[0] == 0)) { 
					if (FileRank.length() == 5) {
						promotion = 'Q';
						Pawn pawn = (Pawn) originPiece;
						pawn.promotion(newP[0], newP[1], board, promotion);
					}
					else {
						promotion = FileRank.charAt(6);
						Pawn pawn = (Pawn) originPiece;
						pawn.promotion(newP[0], newP[1], board, promotion);
					}
				}
				if(!(origin[0] % 2 == 0)) {
					if(!(origin[1] % 2 == 0)) {
						board[origin[0]][origin[1]] = new EmptySpace("   ", false);
					} else {
						board[origin[0]][origin[1]] = new EmptySpace("## ", false);
					}
				} else if(origin[0] % 2 == 0 || origin[0] == 0){
					if(!(origin[1] % 2 == 0)) {
						board[origin[0]][origin[1]] = new EmptySpace("## ", false);
					} else {
						board[origin[0]][origin[1]] = new EmptySpace("   ", false);
					}
				}
				printBoard(board);
			} else {
				System.out.println("Illegal move, try again");
				return false; 
			}
		}	
		return true;
	}
}
