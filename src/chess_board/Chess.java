package chess_board;

import java.util.Scanner;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * The Chess class is where the main method will go and this is what is going to be called at the start of every game. Also handles the inputs and 
 * creating the game. 
 */
public class Chess {
	
	/**
	 * This is where the game will start. 
	 * 
	 * @param String args[]
	 */
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		String input = "";
		boolean draw = false; 
		GameBoard game = new GameBoard();
		game.generateBoard();
		GameBoard.wKPosition[0] = 7; 
		GameBoard.wKPosition[1] = 4;
		GameBoard.bKPosition[0] = 0; 
		GameBoard.bKPosition[1] = 4;
		
		while(true) { 
			
			if(game.GCounter % 2 == 0) {
				System.out.println();
				System.out.println("White's move: ");
				game.whiteMove = true;
				game.blackMove = false;
			} else {
				System.out.println();
				System.out.println("Black's move: ");
				game.blackMove = true;
				game.whiteMove = false;
			}
			
			input = sc.nextLine();
			System.out.println();
			
			if(draw && input.equals("draw")) {
				System.exit(0);
				break; 
			}
			else 
				draw = false;
			
			if(input.contains("draw?"))
				draw = true;  
			
			if(input.equals("resign")) {
				if (game.whiteMove)
					System.out.println("Black wins");
				else 
					System.out.println("White wins");
				System.exit(0);
				break;
			}
			
			if(input != null) {
				boolean successfulMove;
				do {
					successfulMove = game.movePiece(input);
					if(!successfulMove) {
						input = sc.nextLine();
					}
				} while(!successfulMove);
				if (game.blackCheckmate) {
					System.out.println("Checkmate");
					System.out.println("Black wins");
					System.exit(0);
				}
				else if (game.whiteCheckmate) {
					System.out.println("Checkmate");
					System.out.println("White wins");
					System.exit(0);
				}
				if (game.blackMove && game.wIncheck)
					System.out.println("Check");
				else if (game.whiteMove && game.bIncheck)
					System.out.println("Check");
			}
			game.GCounter++;
		}
	}
}
