package chess_pieces;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
* Queen class handles the logic behind the Queen piece and extends the Piece class.
*/
public class Queen extends Piece{

	/**
	 * Initializes a Queen object.
	 * @param name
	 * @param isWhite
	 */
	public Queen(String name, boolean isWhite) {
		super(name, isWhite);
	}
	
	/**
	 * Checks for whether the Queen made a valid move.
	 * @param board
	 * @param origin
	 * @param newP
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {
		if (isSameSpot(origin, newP)) {
			return false; 
		}
		//the following if-branch checks for if horizontal motion
		if (origin[0] == newP[0] && !checkForPiecesInHorizontalPath(origin[1], newP[1], origin[0], board)) {
			if (isWhite && (!board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true; 
			else if (!isWhite && (board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true;
			else 
				return false; 
		}
		//the following checks for if vertical motion
		else if (origin[1] == newP[1] && !checkForPiecesInVerticalPath(origin[0], newP[0], origin[1], board)) {
			if (isWhite && (!board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true; 
			else if (!isWhite && (board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true;
			else 
				return false; 
		}
		else if (Math.abs(origin[0]-newP[0]) == Math.abs(origin[1]-newP[1]) && !checkForPiecesInDiagonalPath(origin[1], newP[1], origin[0], newP[0], board)) {
			if (isWhite && (!board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true; 
			else if (!isWhite && (board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true;
			else 
				return false; 
		}
		else 
			return false;
	}
}
