package chess_pieces;

import chess_board.GameBoard;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * King class handles the logic behind the King piece and extends the Piece class.
 */

public class King extends Piece{
	/**
	 * int blackKingMoveCounter tracks how many times the black king has been moved.
	 */
	int blackKingMoveCounter; 
	/**
	 * int whiteKingMoveCounter tracks how many times the black king has been moved.
	 */
	int whiteKingMoveCounter;
	
	
	/**
	 * Initializes a King object. 
	 * @param name
	 * @param isWhite
	 */
	public King(String name, boolean isWhite) {
		super(name, isWhite);
	}
	
	/**
	 * Checks in all possible directions from the King's POV to see whether it has been checked by an opponent piece. 
	 * @param board
	 * @param rank
	 * @param file
	 * @param isWhite
	 * @return
	 */
	public static boolean checked(Piece[][] board, int rank, int file, boolean isWhite){
		Piece king = board[rank][file];
		king.isWhite = isWhite; 
		Piece opponent;
		int i, j;
		
		/* Check straight lines */
		for (i = rank + 1; i < 8; i++) { //down
			opponent = board[i][file];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				return true;  
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, file, board))
				break;
		}
		
		for (i = rank - 1; i >= 0; i--) { //up
			opponent = board[i][file];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				return true; 
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, file, board))
				break; 
		}
		for (i = file + 1; i < 8; i++) { //right
			opponent = board[rank][i];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				return true;
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(rank, i, board))
				break; 
		}
		for (i = file - 1; i >= 0; i--) { //left
			opponent = board[rank][i];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				return true;
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'R'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(rank, i, board))
				break; 
		}
		
		/* Check diagonals */
		for (i = rank - 1, j = file - 1; GameBoard.inbounds(i, j); i--, j--) { //left-up
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				return true;
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		for (i = rank + 1, j = file - 1; GameBoard.inbounds(i, j); i++, j--) { //left-down
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				return true;
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		for (i = rank - 1, j = file + 1; GameBoard.inbounds(i, j); i--, j++) { //right-up
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				return true;
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		for (i = rank + 1, j = file + 1; GameBoard.inbounds(i, j); i++, j++) { //right-down
			opponent = board[i][j];
			if (opponent.isWhite != king.isWhite && (opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				return true;
			else if (opponent.isWhite != king.isWhite && !(opponent.name.charAt(1) == 'Q' || opponent.name.charAt(1) == 'B'))
				break;
			else if (opponent.isWhite == king.isWhite && !opponent.isEmpty(i, j, board))
				break; 
		}
		
		/* Check for potentially threatening pawns */
		if (king.isWhite) {
			if (GameBoard.inbounds(rank-1, file+1)) {
				if (board[rank-1][file+1].name.equals("bp "))
					return true; 
			}
			if (GameBoard.inbounds(rank-1, file-1)) {
				if (board[rank-1][file-1].name.equals("bp "))
					return true; 
			}
		}
		else {
			if (GameBoard.inbounds(rank+1, file+1)) {
				if (board[rank+1][file+1].name.equals("wp "))
					return true; 
			}
			if (GameBoard.inbounds(rank+1, file-1)) {
				if (board[rank+1][file-1].name.equals("wp "))
					return true; 
			}
		}
		
		/* Check for knights */
		if ((GameBoard.inbounds(rank-2, file+1) && board[rank-2][file+1].name.charAt(1) == 'N' && king.isWhite != board[rank-2][file+1].isWhite) ||
			(GameBoard.inbounds(rank-2, file-1) && board[rank-2][file-1].name.charAt(1) == 'N' && king.isWhite != board[rank-2][file-1].isWhite) ||
			(GameBoard.inbounds(rank+2, file+1) && board[rank+2][file+1].name.charAt(1) == 'N' && king.isWhite != board[rank+2][file+1].isWhite) ||
			(GameBoard.inbounds(rank+2, file-1) && board[rank+2][file-1].name.charAt(1) == 'N' && king.isWhite != board[rank+2][file-1].isWhite) ||
			(GameBoard.inbounds(rank+1, file-2) && board[rank+1][file-2].name.charAt(1) == 'N' && king.isWhite != board[rank+1][file-2].isWhite) ||
			(GameBoard.inbounds(rank-1, file-2) && board[rank-1][file-2].name.charAt(1) == 'N' && king.isWhite != board[rank-1][file-2].isWhite) ||
			(GameBoard.inbounds(rank+1, file+2) && board[rank+1][file+2].name.charAt(1) == 'N' && king.isWhite != board[rank+1][file+2].isWhite) ||
			(GameBoard.inbounds(rank-1, file+2) && board[rank-1][file+2].name.charAt(1) == 'N' && king.isWhite != board[rank-1][file+2].isWhite)) {
			return true; 
		}
		return false; 
	}
	
	
	/**
	 * Checks if the King piece made a valid move and for castling. 
	 * @param board
	 * @param moveCounter
	 * @param origin 
	 * @param newP
	 * @return 
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {	
		if (isSameSpot(origin, newP)) {
			return false; 
		}
		if (isWhite) {
			//conditions for castling 
			if (origin[0]-newP[0] == 0 && Math.abs(origin[1]-newP[1]) == 2 && !checkForPiecesInHorizontalPath(origin[1], newP[1], origin[0], board) && isEmpty(newP[0], newP[1], board) && whiteKingMoveCounter == 0 && !GameBoard.wIncheck) {
				if (origin[1]-newP[1] == -2 && board[7][7].name.equals("wR ")) {
					Rook rook = (Rook) board[7][7];
					if (rook.whiteKingSideRookCounter == 0) {
						board[origin[0]][origin[1] + 1] = rook; 
						board[7][7] = new EmptySpace("   ", false);
						whiteKingMoveCounter++;
						return true; 
					}
					else {
						return false; 
					}
				}
				else if (origin[1]-newP[1] == 2 && board[7][0].name.equals("wR ")) {
					Rook rook = (Rook) board[7][0];
					if (rook.whiteQueenSideRookCounter == 0) {
						board[origin[0]][origin[1] - 1] = rook; 
						board[7][0] = new EmptySpace("## ", false);
						whiteKingMoveCounter++;
						return true; 
					}
					else {
						return false; 
					}
				}
			}
			//King can move one step forward, back, diagonal
			if(origin[0]-newP[0] == 1 && origin[1]-newP[1] == 0) { // 1 step forward
				if(isEmpty(newP[0], newP[1], board) || !board[newP[0]][newP[1]].isWhite) {
					whiteKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			} else if(Math.abs(newP[0]-origin[0]) == 1 && Math.abs(origin[1]-newP[1]) == 1) { // 1 step diagonal 
				if(isEmpty(newP[0], newP[1], board) || !board[newP[0]][newP[1]].isWhite) {
					whiteKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			} else if(origin[0]-newP[0] == -1 && origin[1]-newP[1] == 0) { // 1 step back
				if(isEmpty(newP[0], newP[1], board) || !board[newP[0]][newP[1]].isWhite) {
					whiteKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			} else if(origin[0]-newP[0] == 0 && Math.abs(origin[1]-newP[1]) == 1) { // 1 step side
				if(isEmpty(newP[0], newP[1], board) || !board[newP[0]][newP[1]].isWhite) {
					whiteKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			}
		}
		else {
			//conditions for castling 
			if (origin[0]-newP[0] == 0 && Math.abs(origin[1]-newP[1]) == 2 && !checkForPiecesInHorizontalPath(origin[1], newP[1], origin[0], board) && isEmpty(newP[0], newP[1], board) && blackKingMoveCounter == 0 && !GameBoard.bIncheck) {
				if (origin[1]-newP[1] == -2 && board[0][7].name.equals("bR ")) {
					Rook rook = (Rook) board[0][7];
					if (rook.blackKingSideRookCounter == 0) {
						board[origin[0]][origin[1] + 1] = rook; 
						board[0][7] = new EmptySpace("## ", false);
						blackKingMoveCounter++;
						return true; 
					}
					else {
						return false; 
					}
				}
				else if (origin[1]-newP[1] == 2 && board[0][0].name.equals("bR ")) {
					Rook rook = (Rook) board[0][0];
					if (rook.blackQueenSideRookCounter == 0) {
						board[origin[0]][origin[1] - 1] = rook; 
						board[0][0] = new EmptySpace("   ", false);
						blackKingMoveCounter++;
						return true; 
					}
					else {
						return false; 
					}
				}
			}
			if(origin[0]-newP[0] == -1 && origin[1]-newP[1] == 0) { // 1 step forward
				if(isEmpty(newP[0], newP[1], board) || board[newP[0]][newP[1]].isWhite) {
					blackKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			} else if(Math.abs(newP[0]-origin[0]) == 1 && Math.abs(origin[1]-newP[1]) == 1) { // 1 step diagonal
				if(isEmpty(newP[0], newP[1], board) || board[newP[0]][newP[1]].isWhite) {
					blackKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			} else if(origin[0]-newP[0] == 1 && origin[1]-newP[1] == 0) { // 1 step back
				if(isEmpty(newP[0], newP[1], board) || board[newP[0]][newP[1]].isWhite) {
					blackKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			} else if(origin[0]-newP[0] == 0 && Math.abs(origin[1]-newP[1]) == 1) { // 1 step side
				if(isEmpty(newP[0], newP[1], board) || board[newP[0]][newP[1]].isWhite) {
					blackKingMoveCounter++;
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
}
