package chess_pieces;

import java.util.Scanner;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * Pawn class keeps track of the logic for the Pawn Piece that implements the Piece abstract class.
 */

public class Pawn extends Piece{
	
	/**
	 * Pawn constructor that initializes a Pawn.
	 * @param name
	 * @param isWhite
	 * @param enpassant
	 */
	public Pawn(String name, boolean isWhite, boolean enpassant) {
		super(name, isWhite, enpassant);
	}
	
	/**
	 * Checks is the move for the Pawn is valid. 
	 * @param board
	 * @param origin
	 * @param newP
	 * @return 
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {
		if (isSameSpot(origin, newP)) {
			return false; 
		}
		if (isWhite) {
			if (origin[0]-newP[0] == 2 && origin[1]-newP[1] == 0 && origin[0] == 6) { //if moving two spots forward
				if (isEmpty(origin[0] - 1, origin[1], board) && isEmpty(newP[0], newP[1], board)) {
					return true; 
				}
				else 
					return false; 
			}
			else if (origin[0]-newP[0] == 1 && origin[1]-newP[1] == 0) { //if moving one spot forward
				if (isEmpty(newP[0], newP[1], board))
					return true; 
				else 
					return false; 
			}
			else if (origin[0]-newP[0] == 1 && Math.abs(origin[1]-newP[1]) == 1 && !board[newP[0]][newP[1]].isWhite) { // if moving forward diagonally to capture a black piece
				return true; 
			}
			else if(origin[0]-newP[0] == 1 && Math.abs(origin[1]-newP[1]) == 1 && board[newP[0]+1][newP[1]].name.charAt(0) == 'b' && board[newP[0]+1][newP[1]].enpassant == true) {
				return true;
			}
			else
				return false;
		}
		else { //isBlack
			if (newP[0]-origin[0] == 2 && origin[1]-newP[1] == 0 && origin[0] == 1) { //if moving two spots forward
				if (isEmpty(origin[0] + 1, origin[1], board) && isEmpty(newP[0], newP[1], board)) {
					return true; 
				}
				else 
					return false; 
			}
			else if (newP[0]-origin[0] == 1 && origin[1]-newP[1] == 0) { //if moving one spot forward
				if (isEmpty(newP[0], newP[1], board))
					return true; 
				else 
					return false; 
			}
			else if (newP[0]-origin[0] == 1 && Math.abs(origin[1]-newP[1]) == 1 && board[newP[0]][newP[1]].isWhite) { // if moving forward diagonally to capture white piece
				return true; 
			}
			else if(newP[0]-origin[0] == 1 && Math.abs(origin[1]-newP[1]) == 1 && board[newP[0]-1][newP[1]].name.charAt(0) == 'w' && board[newP[0]-1][newP[1]].enpassant == true) {
				return true;
			}
			else
				return false;
		}
	}
	
	/**
	 * Called when the pawn is valid for a promotion and promotes the Pawn to a queen, rook, bishop or knight. 
	 * @param rank
	 * @param file
	 * @param board
	 */
	public void promotion(int rank, int file, Piece[][] board, char promotion) {
		if (rank == 0) {
				if (promotion == 'Q') {
					board[rank][file] = new Queen("wQ ", true); 
				}
				else if (promotion == 'R') {
					board[rank][file] = new Rook("wR ", true);
				}
				else if (promotion == 'B') {
					board[rank][file] = new Bishop("wB ", true);
				}
				else if (promotion == 'N') {
					board[rank][file] = new Knight("wN ", true);
				}
			}
		else {
			if (promotion == 'Q') {
				board[rank][file] = new Queen("bQ ", false); 
			}
			else if (promotion == 'R') {
				board[rank][file] = new Rook("bR ", false);
			}
			else if (promotion == 'B') {
				board[rank][file] = new Bishop("bB ", false);
			}
			else if (promotion == 'N') {
				board[rank][file] = new Knight("bN ", false);
			}
		}
	}
}
