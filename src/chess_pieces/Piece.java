package chess_pieces;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * The Piece abstract class is what each piece will implement that has common methods that are relevant 
 * for each piece. 
 */

public abstract class Piece {
	
	/**
	 * String name will store the name of the piece.
	 */
	public String name;
	/**
	 * boolean isWhite will keep track if the piece is a white piece or black piece. 
	 */
	public boolean isWhite;
	/**
	 * boolean enpassant will keep track if the pawn is eligible for the enpassant move. 
	 */
	public boolean enpassant;
	
	
	/**
	 * This Piece constructor is to initialize all the Piece objects except for Pawns.
	 * @param name
	 * @param isWhite
	 */
	public Piece(String name, boolean isWhite) {
		this.isWhite = isWhite;
		this.name = name;
	}
	
	/**
	 * This Piece constructor is specifically to initialize the Pawn object.
	 * @param name
	 * @param isWhite
	 * @param enpassant
	 */
	public Piece(String name, boolean isWhite, boolean enpassant) {
		this.isWhite = isWhite;
		this.name = name;
		this.enpassant = enpassant;
	}
	
	/**
	 * Checks if a certain piece made a valid move based on their rules. 
	 * @param board
	 * @param origin
	 * @param newP
	 * @return
	 */
	public abstract boolean isValidMove(Piece[][] board, int origin[], int newP[]);
	
	/**
	 * Checks if a piece did not move based on its origin and newP arrays.
	 * @param origin
	 * @param newP
	 * @return
	 */
	public boolean isSameSpot(int origin[], int newP[]) {
		if(origin[0] == newP[0] && origin[1] == newP[1]) {
			return true;
		}
		else 
			return false; 
	}
	
	/**
	 * isEmpty class checks if a certain location is empty ( "## " or "   ")
	 * @param rank
	 * @param file
	 * @param board
	 * @return
	 */
	public boolean isEmpty(int rank, int file, Piece[][] board) {
		if(board[rank][file].toString().equals("## ") || board[rank][file].toString().equals("   ")) {
			return true; 
		}
		else {
			return false; 
		}
	}
	
	/**
	 * Checks is there is piece in the vertical path of a piece. 
	 * @param startRank
	 * @param endRank
	 * @param file
	 * @param board
	 * @return
	 */
	public boolean checkForPiecesInVerticalPath(int startRank, int endRank, int file, Piece[][] board) {
		if (startRank > endRank) {
			for (int i = startRank - 1; i > endRank; i--) {
				if (isEmpty(i, file, board)) {
					continue; 
				}
				else {
					return true; 
				}
			}
			return false; 
		}
		else {
			for (int i = startRank + 1; i < endRank; i++) {
				if (isEmpty(i, file, board)) {
					continue; 
				}
				else {
					return true; 
				}
			}
			return false; 
		}
	}
	
	/**
	 * Checks for a piece in the horizontal movement of a Piece.
	 * @param startFile
	 * @param endFile
	 * @param rank
	 * @param board
	 * @return
	 */
	public boolean checkForPiecesInHorizontalPath(int startFile, int endFile, int rank, Piece[][] board) {
		if (startFile > endFile) {
			for (int i = startFile - 1; i > endFile; i--) {
				if (isEmpty(rank, i, board)) {
					continue; 
				}
				else {
					return true; 
				}
			}
			return false; 
		}
		else {
			for (int i = startFile + 1; i < endFile; i++) {
				if (isEmpty(rank, i, board)) {
					continue; 
				}
				else {
					return true; 
				}
			}
			return false; 
		}
	}
	
	/**
	 * Checks for a piece in the diagonal path of an Piece. 
	 * @param startFile
	 * @param endFile
	 * @param startRank
	 * @param endRank
	 * @param board
	 * @return
	 */
	public boolean checkForPiecesInDiagonalPath(int startFile, int endFile, int startRank, int endRank, Piece[][] board) {
		int horizontalMove = startFile;
		if (startRank > endRank) {
			if (startFile < endFile) { //diagonal upper-right movement
				for (int i = startRank - 1; i > endRank; i--) {
					horizontalMove++;
					if (isEmpty(i, horizontalMove, board)) {
						continue; 
					}
					else
						return true; 
				} 
				return false; 
			}
			else { //upper-left movement
				for (int i = startRank - 1; i > endRank; i--) {
					horizontalMove--;
					if (isEmpty(i, horizontalMove, board)) {
						continue; 
					}
					else
						return true; 
				}
				return false; 
			}
		}
		else {
			if (startFile < endFile) { //lower-right movement
				for (int i = startRank + 1; i < endRank; i++) {
					horizontalMove++;
					if (isEmpty(i, horizontalMove, board)) {
						continue; 
					}
					else
						return true; 
				} 
				return false; 
			}
			else { //lower-left movement
				for (int i = startRank + 1; i < endRank; i++) {
					horizontalMove--;
					if (isEmpty(i, horizontalMove, board)) {
						continue; 
					}
					else
						return true; 
				}
				return false; 
			}
		}
	}
	
	/**
	 * Returns the name of the piece
	 * @return piece name
	 */
	public String toString() {
		return name; 
	}
}
