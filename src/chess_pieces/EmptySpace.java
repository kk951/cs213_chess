package chess_pieces;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * EmptySpace class stores if a Piece is not a piece and is just a space. Extends the Piece class. 
 */

public class EmptySpace extends Piece{
	
	/**
	 * EmptySpace constructor that initializes the Piece to be an empty space ("## " or "   ")
	 * @param name
	 * @param isWhite
	 */
	public EmptySpace(String name, boolean isWhite) {
		super(name, isWhite);
	}
	
	/**
	 * Checks for a valid move for an EmptySpace which will always be true. 
	 * @param board
	 * @param origin
	 * @param newP
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {
		return true;
	}
}
