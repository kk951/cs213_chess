package chess_pieces;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * Rook class handles the logic behind Rooks in chess and extends the Piece class. 
 */

public class Rook extends Piece{
	
	/**
	 * 
	 */
	int whiteKingSideRookCounter;
	
	/**
	 * 
	 */
	int whiteQueenSideRookCounter;
	
	/**
	 * 
	 */
	int blackKingSideRookCounter;
	
	/**
	 * 
	 */
	int blackQueenSideRookCounter;
	
	/**
	 * Initializes a Rook piece. 
	 * @param name
	 * @param isWhite
	 */
	public Rook(String name, boolean isWhite) {
		super(name, isWhite);
	}
	
	/**
	 * Checks if the move for the Rook is valid or not. 
	 * @param board
	 * @param origin
	 * @param newP
	 * @return 
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {
		if (isSameSpot(origin, newP)) {
			return false; 
		}
		//the following if-branch checks for if horizontal motion
		if (origin[0] == newP[0] && !checkForPiecesInHorizontalPath(origin[1], newP[1], origin[0], board)) {
			if (isWhite && (!board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board))) {
				if (origin[0] == 7 && origin[1] == 7)
					whiteKingSideRookCounter++;
				else if (origin[0] == 7 && origin[1] == 0)
					whiteQueenSideRookCounter++;
				return true; 
			}
			else if (!isWhite && (board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board))) {
				if (origin[0] == 0 && origin[1] == 7)
					blackKingSideRookCounter++;
				else if (origin[0] == 0 && origin[1] == 0)
					blackQueenSideRookCounter++;
				return true;
			}
			else 
				return false; 
		}
		//the following checks for if vertical motion
		else if (origin[1] == newP[1] && !checkForPiecesInVerticalPath(origin[0], newP[0], origin[1], board)) {
			if (isWhite && (!board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board))) {
				if (origin[0] == 7 && origin[1] == 7)
					whiteKingSideRookCounter++;
				else if (origin[0] == 7 && origin[1] == 0)
					whiteQueenSideRookCounter++;
				return true;
			}
			else if (!isWhite && (board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board))) {
				if (origin[0] == 0 && origin[1] == 7)
					blackKingSideRookCounter++;
				else if (origin[0] == 0 && origin[1] == 0)
					blackQueenSideRookCounter++;
				return true;
			}
			else 
				return false; 
		}
		else
			return false;
	}
}
