package chess_pieces;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * Knight class handles the logic behind the Knight piece and extends the Piece class.
 */

public class Knight extends Piece{

	/**
	 * Initializes a Knight object. 
	 * @param name
	 * @param isWhite
	 */
	public Knight(String name, boolean isWhite) {
		super(name, isWhite);
	}
	
	/**
	 * Checks if the Knight made a valid move. 
	 * @param board
	 * @param origin
	 * @param newP
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {
		if (isSameSpot(origin, newP)) {
			return false; 
		}
		int fileDistance = Math.abs(origin[1] - newP[1]);
		int rankDistance = Math.abs(origin[0] - newP[0]);
		if ((fileDistance == 1 && rankDistance == 2) || (fileDistance == 2 && rankDistance == 1)) {
			if (isWhite && (board[newP[0]][newP[1]].name.charAt(0) == 'b' || isEmpty(newP[0], newP[1], board)))
				return true; 
			else if (!isWhite && (board[newP[0]][newP[1]].name.charAt(0) == 'w' || isEmpty(newP[0], newP[1], board)))
				return true;
			else 
				return false; 
		}
		else {
			return false;
		}
	}
}
