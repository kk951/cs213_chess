package chess_pieces;

/**
 * @author Karun Kanda
 * @author Yulin Ni
 */

/**
 * Bishop class that handles the logic behind the Bishop piece and extends the Piece class.
 */
public class Bishop extends Piece{

	/**
	 * Initializes a Bishop object. 
	 * @param name
	 * @param isWhite
	 */
	public Bishop(String name, boolean isWhite) {
		super(name, isWhite);
	}
	
	/**
	 * Checks if the move made by the Bishop is valid. 
	 * @param board
	 * @param origin
	 * @param newP
	 */
	public boolean isValidMove(Piece[][] board, int origin[], int newP[]) {
		if (isSameSpot(origin, newP)) {
			return false; 
		}
		if (Math.abs(origin[0]-newP[0]) == Math.abs(origin[1]-newP[1]) && !checkForPiecesInDiagonalPath(origin[1], newP[1], origin[0], newP[0], board)) {
			if (isWhite && (!board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true; 
			else if (!isWhite && (board[newP[0]][newP[1]].isWhite || isEmpty(newP[0], newP[1], board)))
				return true;
			else 
				return false; 
		}
		else 
			return false;
	}
}
